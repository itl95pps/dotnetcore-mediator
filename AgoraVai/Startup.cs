﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;
using MediatR;
using QueryStack;
using CommandStack;

namespace AgoraVai
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddScoped<IMediator, Mediator>();
            services.AddTransient<SingleInstanceFactory>(sp => t => sp.GetService(t));
            services.AddTransient<MultiInstanceFactory>(sp => t => sp.GetServices(t));
                        
            services.AddMediatorHandlers(typeof(Startup).GetTypeInfo().Assembly);
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        
    }

    public static class MediatorExtensions
    {
        public static IServiceCollection AddMediatorHandlers(this IServiceCollection services, Assembly assembly)
        {
            var listTypes = new List<TypeInfo>();
            listTypes.AddRange(GetAssembliesQuery());
            listTypes.AddRange(GetAssembliesCommand());

            RegisterHandlers(services, listTypes);

            return services;
        }

        private static void RegisterHandlers(IServiceCollection services, IEnumerable<TypeInfo> classTypes)
        {
            foreach (var type in classTypes)
            {
                var interfaces = type.ImplementedInterfaces.Select(i => i.GetTypeInfo());

                foreach (var handlerType in interfaces.Where
                    (i => i.IsGenericType && 
                        (
                            (i.GetGenericTypeDefinition() == typeof(IRequestHandler<,>) ||
                            i.GetGenericTypeDefinition() == typeof(IRequestHandler<>))
                            ||
                            (i.GetGenericTypeDefinition() == typeof(IAsyncRequestHandler<,>) ||
                            i.GetGenericTypeDefinition() == typeof(IAsyncRequestHandler<>))
                        )
                    )
                )
                {
                    services.AddTransient(handlerType.AsType(), type.AsType());
                }
            }
        }

        private static IEnumerable<TypeInfo> GetAssembliesQuery()
        {
            return typeof(UserQueryHandler).GetTypeInfo().Assembly.GetExportedTypes().Select(t => t.GetTypeInfo()).Where(t => t.IsClass && !t.IsAbstract);
        }
        private static IEnumerable<TypeInfo> GetAssembliesCommand()
        {
            return typeof(AddUserCommandHandler).GetTypeInfo().Assembly.GetExportedTypes().Select(t => t.GetTypeInfo()).Where(t => t.IsClass && !t.IsAbstract);
        }
    }
}
