﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using QueryStack;
using CommandStack;
using Domain;

namespace AgoraVai.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator mediator;
        public HomeController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public IActionResult Index()
        {
            mediator.Send(new AddUserCommand(
                new User
                {
                    Login = "itlpps",
                    Password = "qweqew"
                }
            ));
            var model = mediator.Send(new UserQuery());

            mediator.Send(new UpdateUserCommand( 
                new User {
                    Id = 1,
                    Login = "itlpps AEHEHEHEH",
                    Password = "qweqew AOEOEOEOE"
                }
            ));
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
