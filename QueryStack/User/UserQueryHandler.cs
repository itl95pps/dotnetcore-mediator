﻿using Domain;
using MediatR;
using MiniBiggy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryStack
{
    public class UserQuery : IRequest<List<UserProjection>>
    {
    }

    public class UserQueryHandler : IRequestHandler<UserQuery, List<UserProjection>>
    {
        public List<UserProjection> Handle(UserQuery message)
        {
            var list = CreateList<Domain.User>.SavingAt("user.data")
                              .UsingJsonSerializer()
                              .SavingWhenRequested();

            var projectionList = new List<UserProjection>();
            foreach (var item in list)
            {
                projectionList.Add(new UserProjection
                {
                    Id = item.Id,
                    Login = item.Login,
                    Password = item.Password,
                });
            }
            return projectionList;
        }
    }
}
