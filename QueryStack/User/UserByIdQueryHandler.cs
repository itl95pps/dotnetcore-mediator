﻿using MediatR;
using MiniBiggy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryStack.User
{
    public class UserByIdQuery : IRequest<UserProjection>
    {
        public UserByIdQuery(int id)
        {
            this.Id = id;
        }

        public int Id { get; set; }
    }

    public class UserByIdQueryHandler : IRequestHandler<UserByIdQuery, UserProjection>
    {
        public UserProjection Handle(UserByIdQuery message)
        {
            var user = CreateList<Domain.User>.SavingAt("user.data")
                              .UsingJsonSerializer()
                              .SavingWhenRequested()
                              .FirstOrDefault(x=>x.Id.Equals(message.Id));

            return new UserProjection
            {

            };
        }
    }
}
