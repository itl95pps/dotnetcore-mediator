﻿using MediatR;
using MiniBiggy;
using System.Collections.Generic;

namespace CommandStack
{
    public class AddUserCommand : IRequest
    {
        public AddUserCommand(Domain.User user)
        {
            this.User = user;
        }

        public Domain.User User { get; set; }
    }

    public class AddUserCommandHandler : IRequestHandler<AddUserCommand>
    {
        public void Handle(AddUserCommand message)
        {
            var list = CreateList<Domain.User>.SavingAt("user.data")
                               .UsingJsonSerializer()
                               .SavingWhenRequested();

            message.User.Id = list.Count == 0 ? 1 : list.Count + 1;

            list.Add(message.User);
            list.Save();
        }
    }
}
