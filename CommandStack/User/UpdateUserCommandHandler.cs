﻿using MediatR;
using MiniBiggy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandStack
{
    public class UpdateUserCommand : IRequest
    {
        public UpdateUserCommand(Domain.User user)
        {
            this.User = user;
        }

        public Domain.User User { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
    {
        public void Handle(UpdateUserCommand message)
        {

            var list = CreateList<Domain.User>.SavingAt("user.data")
                              .UsingJsonSerializer()
                              .SavingWhenRequested();

            var user = list.FirstOrDefault(x => x.Id.Equals(message.User.Id));

            user.Login = message.User.Login;
            user.Password = message.User.Password;
            
            list.UpdateAsync(user);
            list.Save();
            
        }
    }
}
